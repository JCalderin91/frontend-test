# Frontend knowledge application test

### Framework/Libraries

- ReactJs
- React router
- Context API
- Jest
- React Testing Library
- SCSS

### Install packages

```properties
  npm install
```

### Run application

```properties
  npm start
```

### Run test

```properties
  npm test
```
