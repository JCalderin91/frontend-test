export class Http {
  #baseUrl;
  constructor({ baseUrl }) {
    this.#baseUrl = baseUrl;
  }

  get(url: string) {
    return fetch(`${this.#baseUrl}${url}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => {
        if (!response.ok) throw response;
        return response.json();
      })
      .catch((error) => {
        throw error;
      });
  }

  post(url: string, payload) {
    return fetch(`${this.#baseUrl}${url}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(payload),
    })
      .then((response) => {
        if (!response.ok) throw response;
        return response.json();
      })
      .catch((error) => {
        throw error;
      });
  }

  put(url: string, payload) {
    return fetch(`${this.#baseUrl}${url}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(payload),
    })
      .then((response) => {
        if (!response.ok) throw response;
        return response.json();
      })
      .catch((error) => {
        throw error;
      });
  }

  delete(url: string) {
    return fetch(`${this.#baseUrl}${url}`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => {
        if (!response.ok) throw response;
        return response.json();
      })
      .catch((error) => {
        throw error;
      });
  }
}
