import { Post, PostState } from "interfaces/Index";
import { createContext } from "react";

export type PostContextProps = {
  postState: PostState;
  addPost(post: Post): void;
  addPost(post: Post): void;
  removePost(postId: string | number): void;
  editPost(post: Post): void;
  setPost(post: Post): void;
  getPosts(): void;
};

export const PostContext = createContext<PostContextProps>(
  {} as PostContextProps
);
