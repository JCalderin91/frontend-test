import { Post, PostState } from "interfaces/Index";

type PostAction =
  | { type: "addPosts"; payload: Post[] }
  | { type: "addPost"; payload: Post }
  | { type: "removePost"; payload: number | string }
  | { type: "editPost"; payload: Post }
  | { type: "setPost"; payload: Post };

export const postReducer = (
  state: PostState,
  action: PostAction
): PostState => {
  switch (action.type) {
    case "addPosts":
      return {
        ...state,
        posts: action.payload,
      };
    case "addPost":
      return {
        ...state,
        posts: [
          ...state.posts,
          {
            ...action.payload,
            date: new Date(),
          },
        ],
      };
    case "removePost":
      return {
        ...state,
        posts: state.posts.filter(({ id }) => id !== action.payload),
      };
    case "editPost":
      return {
        ...state,
        posts: state.posts.map(({ ...post }) => {
          if (post.id === action.payload.id) return action.payload;
          return post;
        }),
      };
    case "setPost":
      return {
        ...state,
        post: action.payload,
      };

    default:
      return state;
  }
};
