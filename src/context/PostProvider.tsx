import { PostState } from "interfaces/Index";
import { useReducer, useMemo } from "react";
import { PostService } from "services/Post";
import { PostContext } from "./PostContext";
import { postReducer } from "./postReducer";

const INITIAL_STATE: PostState = {
  posts: [],
  post: {
    author: "",
    description: "",
    title: "",
  },
};

interface props {
  children: JSX.Element | JSX.Element[];
}

const PostProvider = ({ children }: props) => {
  const [postState, dispatch] = useReducer(postReducer, INITIAL_STATE);

  const getPosts = useMemo(
    () => async () => {
      try {
        const posts = await PostService.getAll();
        dispatch({
          type: "addPosts",
          payload: posts,
        });
      } catch (error) {
        throw error;
      }
    },
    []
  );

  const addPost = useMemo(
    () => async (post) => {
      try {
        const newPost = await PostService.create(post);
        dispatch({ type: "addPost", payload: newPost });
      } catch (error) {
        throw error;
      }
    },
    []
  );

  const removePost = useMemo(
    () => async (postId) => {
      try {
        await PostService.destroy(postId);
        dispatch({ type: "removePost", payload: postId });
      } catch (error) {
        throw error;
      }
    },
    []
  );

  const editPost = useMemo(
    () => async (post) => {
      try {
        await PostService.update(post);
        dispatch({ type: "editPost", payload: post });
      } catch (error) {
        throw error;
      }
    },
    []
  );

  const setPost = useMemo(
    () => (post) => {
      dispatch({ type: "setPost", payload: post });
    },
    []
  );

  return (
    <PostContext.Provider
      value={{ postState, addPost, removePost, editPost, setPost, getPosts }}
    >
      {children}
    </PostContext.Provider>
  );
};
export default PostProvider;
