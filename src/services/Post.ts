import { Post } from "interfaces/Index";
import { Http } from "../Http";

const http = new Http({ baseUrl: process.env.REACT_APP_BASE_URL });

export const PostService = {
  getAll: () => {
    return http.get(`/posts`);
  },

  create: (post: Post) => {
    return http.post(`/posts`, post).catch((error) => {
      throw error;
    });
  },

  update: (post: Post) => {
    const { id, title, description, author } = post;
    return http.put(`/posts/${id}`, { title, description, author });
  },

  destroy: (id: string | number) => {
    return http.delete(`/posts/${id}`);
  },
};
