import { Link } from "react-router-dom";
import underConstructionImage from "assets/images/under-construction.png";
import "./index.scss";

export default function UnderConstruction({ page }) {
  return (
    <div className="under-construction">
      <div>
        <img
          src={underConstructionImage}
          alt="Under Construction"
          className="under-construction-image"
        />
        <h1>We are sorry</h1>
        <h2>This site "{page}" is under construction</h2>
        But you can check out our <Link to="/">home</Link> page
      </div>
    </div>
  );
}
