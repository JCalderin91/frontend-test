import "./styles.scss";
const defaultImage = require("assets/images/posts/post-4.png");

interface props {
  title: string;
  author: string;
  description: string;
}

export default function Article({ title, author, description }: props) {
  return (
    <article className="article">
      <picture>
        <img
          className="article__picture"
          src={defaultImage}
          alt={`post by ${author}`}
        />
      </picture>
      <div className="article__content">
        <div className="article__author">By {author}</div>
        <div className="article__title">{title}</div>
        <div className="article__description">{description}</div>
      </div>
    </article>
  );
}
