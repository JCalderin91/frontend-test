import "@testing-library/jest-dom/extend-expect";
import { render, screen } from "@testing-library/react";

import Article from "./index";


describe('Article component', () => {
  const articleMock = {
    image: "http://image/image.png",
    author: "Jcalderin",
    title: "My post",
    description: "A little description",
  };
  let article;
  beforeEach(() => {
    render(
      <Article
        title={articleMock.title}
        author={articleMock.author}
        image={articleMock.image}
        description={articleMock.description}
      />
    );
  })

  test("Should work as spected", () => {
    article = screen.getByRole("article");
    expect(article).toBeInTheDocument();
    expect(article).toHaveTextContent(articleMock.title);
    expect(article).toHaveTextContent(articleMock.author);
    expect(article).toHaveTextContent(articleMock.description);
  });

  test("Should can be see a image of article", () => {
    const image = screen.getByRole('img', { name: /post by Jcalderin/i })
    expect(image).toBeInTheDocument()
  })
})
