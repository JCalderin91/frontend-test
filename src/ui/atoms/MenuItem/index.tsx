import { Link, useLocation } from "react-router-dom";
import "./index.scss";

export default function MenuItem({ children, to }) {
  const location = useLocation();
  const isActive = location.pathname === to;
  return (
    <Link to={to} className={`menu-item ${isActive ? "active" : ""}`}>
      {children}
    </Link>
  );
}
