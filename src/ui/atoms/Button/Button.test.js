import '@testing-library/jest-dom/extend-expect'
import { render, screen } from "@testing-library/react";

import Button from "./index";

test('Should work as spected', () => {
  render(<Button>accept</Button>)
  const button = screen.getByRole('button')
  expect(button).toBeInTheDocument()
  expect(button).toHaveTextContent(/accept/i)
})