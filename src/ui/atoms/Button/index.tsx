import "./styles.scss";

export default function Button({ children, ...attrs }) {
  const className = `button ${attrs.className ? attrs.className : ""}`;

  return (
    <button {...attrs} className={className}>
      <div className="button__content">{children}</div>
    </button>
  );
}
