import { IconType } from "interfaces/Index";
import Facebook from "ui/atoms/Icon/icons/Facebook";
import Instagram from "ui/atoms/Icon/icons/Instagram";
import Pinterest from "ui/atoms/Icon/icons/Pinterest";
import Twitter from "ui/atoms/Icon/icons/Twitter";
import Youtube from "ui/atoms/Icon/icons/Youtube";

interface props {
  type: IconType;
}

const Icon = ({ type = "Facebook" }: props) => {
  switch (type) {
    case "Facebook":
      return <Facebook />;
    case "Instagram":
      return <Instagram />;
    case "Pinterest":
      return <Pinterest />;
    case "Twitter":
      return <Twitter />;
    case "Youtube":
      return <Youtube />;
    default:
      return <Facebook />;
  }
};
export default Icon;
