import { Post } from "interfaces/Index";
import React, { useState } from "react";

interface props {
  post: Post;
  setPost(post: Post): void;
  loading: boolean;
  postOnEditId: string | number;
  removePost(postId: string | number);
}

const TableItem = ({
  post,
  setPost,
  loading,
  postOnEditId,
  removePost,
}: props) => {
  const [isDeleting, setDeleting] = useState(false);

  const formatDate = (date) => {
    const localeDate = new Date(date);
    return localeDate.toLocaleDateString("en-US");
  };

  const handleRemove = async () => {
    setDeleting(true);
    try {
      if (post.id) await removePost(post.id);
    } catch (error) {
      console.error(error);
    }
    setDeleting(false);
  };
  return (
    <tr key={post.id}>
      <td>{post.author}</td>
      <td>{post.title}</td>
      <td>{post.description}</td>
      <td>{formatDate(post.date)}</td>
      <td>
        <button
          disabled={loading}
          className="btn-edit"
          onClick={() => setPost(post)}
        >
          Edit
        </button>
        <button
          disabled={loading || postOnEditId === post.id || isDeleting}
          className="btn-delete"
          onClick={handleRemove}
        >
          Delete
        </button>
      </td>
    </tr>
  );
};
export default React.memo(TableItem);
