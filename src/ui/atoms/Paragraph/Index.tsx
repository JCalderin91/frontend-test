import "./styles.scss";

export default function Paragraph({ children, ...attrs }) {
  return (
    <p className="subtitle" {...attrs}>
      {children}
    </p>
  );
}
