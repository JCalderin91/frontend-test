import React, { ReactNode } from "react";

type HeadingTag = "h1" | "h2" | "h3" | "h4" | "h5" | "h6";

interface props {
  level: HeadingTag;
  children: ReactNode;
  className?: string;
}

function Heading({ level: Tag = "h1", children, className }: props) {
  return <Tag className={className}>{children}</Tag>;
}
export default React.memo(Heading);
