import { Link } from "react-router-dom";
import logoLight from "assets/images/logo-light.svg";
// import SocialNetworks from "ui/molecules/SocialNetworks";
import Button from "ui/atoms/Button";
import "./index.scss";
import Icon from "ui/atoms/Icon/Index";
import Paragraph from "ui/atoms/Paragraph/Index";
import { IconType } from "interfaces/Index";

interface SocialNetworks {
  icon: IconType;
  alt: string;
  title: string;
  href: string;
}

export default function Footer() {
  const socialNetworks: SocialNetworks[] = [
    {
      icon: "Facebook",
      alt: "Facebook icon",
      title: "Facebook",
      href: "http://www.facebook.com",
    },
    {
      alt: "Youtube icon",
      title: "Youtube",
      icon: "Youtube",
      href: "http://www.youtube.com",
    },
    {
      alt: "Twitter icon",
      title: "Twitter",
      icon: "Twitter",
      href: "http://www.twitter.com",
    },
    {
      alt: "Pinterest icon",
      title: "Pinterest",
      icon: "Pinterest",
      href: "http://www.pinterest.com",
    },
    {
      alt: "Instagram icon",
      title: "Instagram",
      icon: "Instagram",
      href: "http://www.instagram.com",
    },
  ];

  return (
    <div className="footer">
      <div className="container">
        <div className="footer__wrapper">
          <div className="logo-section">
            <Link to="/" className="logo-link">
              <img
                src={logoLight}
                alt="easybank logo light"
                className="logo-img"
                height="25"
              />
            </Link>
            <div className="socialNetworks">
              {socialNetworks.map((socialNetwork, key) => {
                return (
                  <a
                    key={key}
                    className="socialNetworks__icon"
                    href={socialNetwork.href}
                    title={socialNetwork.title}
                    target="_blank"
                    rel="noreferrer"
                  >
                    <Icon type={socialNetwork.icon} />
                  </a>
                );
              })}
            </div>
          </div>
          <div className="links-section">
            <ul className="link-group">
              <li>
                <Link to="/about" className="link">
                  About Us
                </Link>
              </li>
              <li>
                <Link to="/contact" className="link">
                  Contact
                </Link>
              </li>
              <li>
                <Link to="/blog" className="link">
                  Blog
                </Link>
              </li>
              <li>
                <Link to="/careers" className="link">
                  Careers
                </Link>
              </li>
              <li>
                <Link to="/support" className="link">
                  Support
                </Link>
              </li>
              <li>
                <Link to="/privacy-policy" className="link">
                  Privacy Policy
                </Link>
              </li>
            </ul>
          </div>
          <div className="request-section">
            <Button>Request Invite</Button>
            <Paragraph className="copyright">
              © Easybank. All Rights Reserved
            </Paragraph>
          </div>
        </div>
      </div>
    </div>
  );
}
