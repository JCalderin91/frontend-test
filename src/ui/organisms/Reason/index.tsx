import Paragraph from "ui/atoms/Paragraph/Index";
import "./index.scss";

interface Props {
  icon: string;
  title: string;
  description: string;
}

export default function Reason({ icon, title, description }: Props) {
  return (
    <div className="rason">
      <div className="reason__icon">
        <img src={icon} alt={title} className="reason__icon__image" />
      </div>
      <h5 className="reason__title">{title}</h5>
      <Paragraph className="reason__description">{description}</Paragraph>
    </div>
  );
}
