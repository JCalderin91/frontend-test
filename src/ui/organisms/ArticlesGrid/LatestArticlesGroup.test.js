import React from "react";
import "@testing-library/jest-dom/extend-expect";
import { render, screen } from "@testing-library/react";

import ArticlesGrid from "./index";

describe('LatestArticlesGroup component', () => {

  describe('When given a empty list', () => {
    test("Should see a empty list message", () => {
      render(<ArticlesGrid articles={[]} />)
      const emtyListMessage = screen.queryByText('No articles to show')
      expect(emtyListMessage).toBeInTheDocument()
    });
  })

  describe('Given a valid list of articles', () => {
    test("Show a list of articles", async () => {
      const data = [
        {
          image: 'http://image/image.png',
          author: 'Jcalderin',
          title: 'My post',
          description: 'A little description'
        },
      ]
      render(<ArticlesGrid articles={data} />);
      const articles = screen.getAllByRole('article')
      expect(articles).toHaveLength(1)

    })
  })

});
