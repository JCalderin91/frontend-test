import { Post } from "interfaces/Index";
import Article from "ui/atoms/Article";
import Paragraph from "ui/atoms/Paragraph/Index";

import "./index.scss";

interface props {
  max: number;
  articles: Post[];
}

function ArticlesGrid({ max, articles }: props) {
  if (!articles.length)
    return <Paragraph className="no-articles">No articles to show</Paragraph>;

  let articlesFiltered = articles;
  if (max) articlesFiltered = articlesFiltered.slice(0, max);

  return (
    <section className="article-group">
      {articlesFiltered.map((article, key) => {
        const { title, description, author } = article;
        return (
          <Article
            title={title}
            description={description}
            author={author}
            key={key}
          />
        );
      })}
    </section>
  );
}

export default ArticlesGrid;
