import bgDesktop from "assets/images/bg-desktop.png";
import mockups from "assets/images/mockups.png";
import logoDark from "assets/images/logo-dark.svg";

import Button from "ui/atoms/Button";
import Paragraph from "ui/atoms/Paragraph/Index";
import Heading from "ui/atoms/Heading/Index";

import "./styles.scss";

export default function Hero() {
  return (
    <div className="hero">
      <img className="hero__bg-desktop" src={bgDesktop} alt="bg-desktop" />
      <img className="hero__mockups" src={mockups} alt="mockups" />

      <div className="hero__container">
        <img
          className="hero__logo"
          src={logoDark}
          alt="easybank logo dark"
          width={256}
          height={40}
        />

        <Heading level="h2" className="hero__main-text">
          Next generation digital banking
        </Heading>

        <Paragraph className="hero__subtitle">
          Take your financial life online. Your Easybank account
          <br /> will be a one-stop-shop for spending, saving. budgeting,
          nvesting, and much more.
        </Paragraph>

        <Button>Request invite</Button>
      </div>
    </div>
  );
}
