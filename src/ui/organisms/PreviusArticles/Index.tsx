import React, { useState, useEffect } from "react";
import { Post } from "interfaces/Index";

import Table from "ui/molecules/Table";
import Pagination from "ui/molecules/Pagination";
import Heading from "ui/atoms/Heading/Index";
import Paragraph from "ui/atoms/Paragraph/Index";

import "./syles.scss";

interface props {
  articles: Post[];
}

function PreviusArticles({ articles }: props) {
  const [itemsPerPage] = useState(4);

  const [page, setPage] = useState(1);
  const [pageCount, setPageCount] = useState(1);

  useEffect(() => {
    if (page > pageCount && page > 1) {
      setPage(pageCount);
    }
  }, [pageCount, page]);

  useEffect(() => {
    setPageCount(Math.ceil(articles.length / itemsPerPage));
  }, [articles, itemsPerPage]);

  const prevPage = () => {
    if (page > 1) setPage(page - 1);
  };
  const nextPage = () => {
    if (page < pageCount) setPage(page + 1);
  };

  return (
    <div className="previusArticles">
      <Heading level="h2">Previous Articles</Heading>
      <Paragraph>
        Review and edit previous blog posts published on to the homepage.
      </Paragraph>
      <Table
        loading={false}
        articles={articles}
        itemsPerPage={itemsPerPage}
        currentPage={page}
      />
      <Pagination
        next={nextPage}
        prev={prevPage}
        currentPage={page}
        pageCount={pageCount}
        setPage={setPage}
      />
    </div>
  );
}

export default React.memo(PreviusArticles);
