import { Link } from "react-router-dom";
import logoDark from "assets/images/logo-dark.svg";
import closeIcon from "assets/images/close-icon.svg";
import "./index.scss";

export default function MobileMenu({ activeMenu, toggleMenu }) {
  return (
    <div className={`mobile-menu ${activeMenu ? "active" : ""}`}>
      <div className="mobile-menu__header">
        <div className="mobile-menu__header-logo">
          <img src={logoDark} alt="easybank logo" height="25" width={160} />
        </div>
        <div className="mobile-menu__header-close" onClick={toggleMenu}>
          <img src={closeIcon} alt="close icon" height="25" width="25" />
        </div>
      </div>
      <div className="mobile-menu__content">
        <div className="mobile-menu__content-item">
          <Link className="link" onClick={toggleMenu} to="/">
            Home
          </Link>
        </div>
        <div className="mobile-menu__content-item">
          <Link className="link" onClick={toggleMenu} to="/about">
            About
          </Link>
        </div>
        <div className="mobile-menu__content-item">
          <Link className="link" onClick={toggleMenu} to="/contact">
            Contact
          </Link>
        </div>
        <div className="mobile-menu__content-item">
          <Link className="link" onClick={toggleMenu} to="/blog">
            Blog
          </Link>
        </div>
        <div className="mobile-menu__content-item">
          <Link className="link" onClick={toggleMenu} to="/careers">
            Careers
          </Link>
        </div>
        <div className="mobile-menu__content-item">
          <Link className="link" onClick={toggleMenu} to="/support">
            Support
          </Link>
        </div>
        <div className="mobile-menu__content-item">
          <Link className="link" onClick={toggleMenu} to="/privacy-policy">
            Privacy Policy
          </Link>
        </div>
      </div>
    </div>
  );
}
