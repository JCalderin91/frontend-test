import { ChangeEvent, FormEvent, useCallback } from "react";
import { usePost } from "hooks/usePosts";
import { useState, useEffect } from "react";
import Button from "ui/atoms/Button";
import "./index.scss";

interface Errors {
  title: string;
  author: string;
  description: string;
}

export default function Form() {
  const { addPost, post, setPost, editPost } = usePost();

  const [isValid, setIsValid] = useState(false);
  const [isSaving, setSaving] = useState(false);

  const [errors, setErrors] = useState<Errors>({} as Errors);

  useEffect(() => {
    setErrors({
      author: "",
      description: "",
      title: "",
    });

    setIsValid(true);

    if (post.author?.length < 3 || post.author?.length > 50) {
      if (post.author?.length)
        setErrors({
          ...errors,
          author: "Author name must be between 3 and 50 characters",
        });
      setIsValid(false);
    }
    if (post.title.length < 3 || post.title?.length > 50) {
      if (post.title?.length)
        setErrors({
          ...errors,
          title: "Title must be between 3 and 50 characters",
        });
      setIsValid(false);
    }
    if (post.description?.length < 3 || post.description?.length > 50) {
      if (post.description?.length)
        setErrors({
          ...errors,
          description: "Content must be between 3 and 50 characters",
        });
      setIsValid(false);
    }
    if (
      !post.author?.length ||
      !post.title?.length ||
      !post.description?.length
    ) {
      setIsValid(false);
    }
  }, [post]);

  const inputHandler = ({
    target,
  }: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const { name, value } = target;
    setPost({ ...post, [name]: value });
  };

  const submitHandler = async (ev: FormEvent<HTMLFormElement>) => {
    ev.preventDefault();
    setSaving(true);
    try {
      if (post.id) await editPost(post);
      else await addPost(post);
      clearForm();
    } catch (error) {
      console.log(error);
    }
    setSaving(false);
  };

  const clearForm = useCallback(
    () =>
      setPost({
        author: "",
        title: "",
        description: "",
      }),
    [setPost]
  );

  useEffect(() => clearForm(), [clearForm]);

  return (
    <form className="form" onSubmit={submitHandler}>
      <div className={`field ${errors.author && "field--has-error"}`}>
        <label className="field__label" htmlFor="author">
          Author
        </label>
        <input
          className="field__input"
          type="text"
          id="author"
          name="author"
          value={post.author}
          autoComplete="off"
          onChange={inputHandler}
        />
        <span className="field__error">{errors.author}</span>
      </div>

      <div className={`field ${errors.title && "field--has-error"}`}>
        <label className="field__label" htmlFor="title">
          Title
        </label>
        <input
          className="field__input"
          type="text"
          id="title"
          name="title"
          value={post.title}
          autoComplete="off"
          onChange={inputHandler}
        />
        <span className="field__error">{errors.title}</span>
      </div>

      <div className={`field ${errors.description && "field--has-error"}`}>
        <label className="field__label" htmlFor="description">
          Description
        </label>
        <textarea
          className="field__input"
          id="description"
          name="description"
          value={post.description}
          onChange={inputHandler}
          autoComplete="off"
          rows={4}
        />
        <span className="field__error">{errors.description}</span>
      </div>

      <div className="form__actions">
        <Button type="button" onClick={clearForm} className="small text">
          {!post.id ? "Clear form" : "Cancelar"}
        </Button>

        <Button type="submit" disabled={!isValid || isSaving} className="small">
          {!post.id ? "Save" : "Update"}
        </Button>
      </div>
    </form>
  );
}
