import "@testing-library/jest-dom/extend-expect";
import { fireEvent, render, screen } from "@testing-library/react";

import Form from "./index";
describe('Form component', () => {
  test("Should work as expected", () => {
    const post = {
      image: '',
      author: '',
      title: '',
      description: ''
    }
    render(<Form post={post} />);
    const authorInput = screen.getByLabelText('Author')
    expect(authorInput).toBeInTheDocument()

    const titleInput = screen.getByLabelText('Title')
    expect(titleInput).toBeInTheDocument()

    const descriptionTextarea = screen.getByLabelText('Description')
    expect(descriptionTextarea).toBeInTheDocument()
  });

  describe("Given a invalid data", () => {
    test('Should see an errors messages', () => {
      const post = {
        image: '',
        author: 'j',
        title: 'a',
        description: 's'
      }
      const view = render(<Form post={post} setPost={() => { }} />)

      const authorInput = screen.getByLabelText('Author')
      fireEvent.input(authorInput, { target: { value: 'je' } })
      view.debug()

      const authorErrorMessage = screen.getByText('Author name must be between 3 and 50 characters')
      expect(authorErrorMessage).toBeInTheDocument()

      const titleErrorMessage = screen.getByText('Title must be between 3 and 50 characters')
      expect(titleErrorMessage).toBeInTheDocument()

      const descriptionErrorMessage = screen.getByText('Description must be between 3 and 50 characters')
      expect(descriptionErrorMessage).toBeInTheDocument()
    })
  });
})


