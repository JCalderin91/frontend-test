import { Link } from "react-router-dom";

import logoDark from "assets/images/logo-dark.svg";
import burgerMenu from "assets/images/burger-menu.svg";

import Menu from "ui/molecules/Menu";
import Button from "ui/atoms/Button";

import "./index.scss";

export default function Navbar({ toggleMenu }) {
  return (
    <div className="navbar">
      <div className="container">
        <Link to="/">
          <img
            src={logoDark}
            alt="easybank logo light"
            height="25"
            width={160}
          />
        </Link>
        <div className="only-desktop">
          <span />
          <Menu />
          <Button>Request Invite</Button>
        </div>
        <div className="only-mobile">
          <div className="burger-menu" onClick={toggleMenu}>
            <img
              src={burgerMenu}
              alt="burger-menu icon"
              width={20}
              height={20}
              className="burger-menu__img"
            />
          </div>
        </div>
      </div>
    </div>
  );
}
