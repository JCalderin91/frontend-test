import Paragraph from "ui/atoms/Paragraph/Index";
import Heading from "ui/atoms/Heading/Index";
import Reason from "ui/organisms/Reason";
import "./index.scss";

const reasons = [
  {
    icon: require("assets/images/icons/online-banking.png"),
    title: "Online Banking",
    description:
      "Our modern web and mobile applications allow you to keep track of your finances wherever you are in the world. ",
  },
  {
    icon: require("assets/images/icons/simple-budgeting.png"),
    title: "Simple Budgeting",
    description:
      "See exactly where your money goes each month. Receive notifications when you’re close to hitting your limits.",
  },
  {
    icon: require("assets/images/icons/fast-onboarding.png"),
    title: "Fast Onboarding",
    description:
      "We don’t do branches. Open your account in minutes online and start taking control of your finances right away. ",
  },
  {
    icon: require("assets/images/icons/api.png"),
    title: "Open API",
    description:
      "Manage your savings, investments, pension, and much more from one account. Tracking your money has never been easier. ",
  },
];

const reasonToRender = reasons.map(({ icon, title, description }, key) => (
  <Reason icon={icon} title={title} description={description} key={key} />
));

export default function WhyChoose() {
  return (
    <div className="why-choose">
      <div className="container">
        <Heading level="h2" className="why-choose__title">
          Why choose Easybank?
        </Heading>
        <Paragraph className="why-choose__subtitle">
          We leverage Open Banking to turn your bank acount into your financial
          hub. Control your finances like never before.
        </Paragraph>
        <div className="why-choose__group">{reasonToRender}</div>
      </div>
    </div>
  );
}
