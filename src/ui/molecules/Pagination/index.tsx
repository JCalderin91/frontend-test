import leftIcon from "assets/images/icons/pagination/left.png";
import rightIcon from "assets/images/icons/pagination/right.png";

import "./index.scss";

interface Props {
  next: () => void;
  prev: () => void;
  currentPage: number;
  pageCount: number;
  setPage: (page: number) => void;
}

export default function Pagination({
  next,
  prev,
  currentPage,
  pageCount,
  setPage,
}: Props) {
  return (
    <div className="paginate">
      <div className="paginate__prev" onClick={prev}>
        <img src={leftIcon} alt="left icon" />
        Previous
      </div>
      <div className="paginate__indicator-group">
        {[...Array(Number(pageCount))].map((x, i) => (
          <div
            key={i}
            className={`paginate__indicator-group-number ${
              i + 1 === currentPage ? "active" : ""
            }`}
            onClick={() => setPage(i + 1)}
          >
            {i + 1}
          </div>
        ))}
      </div>
      <div className="paginate__next" onClick={next}>
        Next
        <img src={rightIcon} alt="left icon" />
      </div>
    </div>
  );
}
