import { usePost } from "hooks/usePosts";

import "./index.scss";
import TableItem from "ui/atoms/TableItem/Index";
import { Post } from "interfaces/Index";

interface Props {
  loading: boolean;
  articles: Post[];
  currentPage: number;
  itemsPerPage: number;
}

function Table({ loading, articles, currentPage, itemsPerPage }: Props) {
  const {
    removePost,
    setPost,
    post: { id },
  } = usePost();

  const postOnEditId = id || 0;

  const postsPaginated = articles.slice(
    (currentPage - 1) * itemsPerPage,
    currentPage * itemsPerPage
  );

  return (
    <div className={`articles-table ${loading ? "loading" : ""}`}>
      <table className="articles-table">
        <thead>
          <tr>
            <th>Author name</th>
            <th>Title</th>
            <th>Description</th>
            <th>Date</th>
            <th>Actions</th> {/* edit and delete buttons */}
          </tr>
        </thead>
        <tbody>
          {postsPaginated.length > 0 ? (
            postsPaginated.map((post, key) => {
              return (
                <TableItem
                  key={key}
                  post={post}
                  setPost={setPost}
                  loading={loading}
                  postOnEditId={postOnEditId}
                  removePost={removePost}
                />
              );
            })
          ) : (
            <tr>
              <td colSpan={5} className="text-center">
                No articles
              </td>
            </tr>
          )}
        </tbody>
      </table>
    </div>
  );
}

export default Table;
