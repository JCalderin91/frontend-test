import { Link } from "react-router-dom";
import { usePost } from "hooks/usePosts";

import Button from "ui/atoms/Button";
import ArticlesGrid from "ui/organisms/ArticlesGrid";
import Heading from "ui/atoms/Heading/Index";

import "./index.scss";

export default function LatestArticles() {
  const { posts } = usePost();

  return (
    <div className="latest-articles container">
      <header className="latest-articles__header">
        <Heading level="h2" className="latest-articles__header__title">
          Latest Articles
        </Heading>
        <Link to="/blog" className="decoration-none">
          <Button size="small" className="latest-articles__header__button">
            <span className="latest-articles__header__button__icon">+</span>
            <span className="latest-articles__header__button__text">
              Add New Article
            </span>
          </Button>
        </Link>
      </header>
      <ArticlesGrid max={4} articles={posts} />
    </div>
  );
}
