import MenuItem from "ui/atoms/MenuItem";
import "./index.scss";
export default function Menu() {
  return (
    <div className="menu">
      <MenuItem to="/">Home</MenuItem>
      <MenuItem to="/about">About</MenuItem>
      <MenuItem to="/contact">Contact</MenuItem>
      <MenuItem to="/blog">Blog</MenuItem>
      <MenuItem to="/careers">Careers</MenuItem>
    </div>
  );
}
