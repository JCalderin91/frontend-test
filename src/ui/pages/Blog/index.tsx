import { useEffect } from "react";
import Form from "ui/organisms/Form";
import PreviusArticles from "ui/organisms/PreviusArticles/Index";
import ArticlesGrid from "ui/organisms/ArticlesGrid";

import "./index.scss";
import Paragraph from "ui/atoms/Paragraph/Index";
import Heading from "ui/atoms/Heading/Index";
import { usePost } from "hooks/usePosts";

export default function Blog() {
  const { posts, isEditing, getPosts } = usePost();

  useEffect(() => {
    getPosts();
  }, [getPosts]);

  return (
    <div className="Blog">
      <div className="container">
        <header>
          <Heading level="h2">
            {isEditing ? "Editing" : "Add new"} blog Article
          </Heading>
          <Paragraph className="Blog__subtitle">
            {isEditing ? "Edit a" : "Publish a new"} blog article to feature in
            the Easybank homepage.
          </Paragraph>
        </header>

        <Form />

        <PreviusArticles articles={posts} />

        <Heading level="h2" className="Blog__title--secondary">
          Latest Articles
        </Heading>

        <ArticlesGrid max={4} articles={posts} />
      </div>
    </div>
  );
}
