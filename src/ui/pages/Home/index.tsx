import { useEffect } from "react";

import Hero from "ui/organisms/Hero";
import LatestArticles from "ui/molecules/LatestArticles";
import WhyChoose from "ui/organisms/WhyChoose";

import "./style.scss";
import { usePost } from "hooks/usePosts";

export default function Home() {
  const { getPosts } = usePost();

  useEffect(() => {
    getPosts();
  }, [getPosts]);

  return (
    <div className="Home">
      <Hero />
      <WhyChoose />
      <LatestArticles />
    </div>
  );
}
