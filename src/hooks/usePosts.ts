import { PostContext } from "context/PostContext";
import { Post } from "interfaces/Index";
import { useContext } from "react";

interface iUsePost {
  posts: Post[];
  post: Post;
  isEditing: boolean;
  addPost(post: Post): void;
  removePost(postId: string | number): void;
  editPost(post: Post): void;
  setPost(post: Post): void;
  getPosts(): void;
}

export const usePost = (): iUsePost => {
  const { postState, addPost, removePost, editPost, setPost, getPosts } =
    useContext(PostContext);

  return {
    posts: postState.posts,
    post: postState.post,
    isEditing: !!postState.post.id,
    addPost,
    removePost,
    editPost,
    setPost,
    getPosts,
  };
};
