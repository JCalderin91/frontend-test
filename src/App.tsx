import { useState } from "react";
import { Routes, Route } from "react-router-dom";

import Footer from "ui/organisms/Footer";
import Navbar from "ui/organisms/Navbar";
import MobileMenu from "ui/organisms/MobileMenu";
import About from "ui/pages/About";
import Contact from "ui/pages/Contact";
import Home from "ui/pages/Home";
import Blog from "ui/pages/Blog";
import Careers from "ui/pages/Careers";
import Support from "ui/pages/Support";
import PrivacyPolicy from "ui/pages/PrivacyPolicy";
import ScrollToTop from "ui/atoms/ScrollTotop";
import PostProvider from "context/PostProvider";

import "./App.scss";

function App() {
  const [activeMenu, setActiveMenu] = useState(false);
  return (
    <main className={`main ${activeMenu ? "menu-open" : ""}`}>
      <Navbar toggleMenu={() => setActiveMenu(!activeMenu)} />
      <MobileMenu
        activeMenu={activeMenu}
        toggleMenu={() => setActiveMenu(!activeMenu)}
      />
      <ScrollToTop>
        <PostProvider>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="about" element={<About />} />
            <Route path="contact" element={<Contact />} />
            <Route path="blog" element={<Blog />} />
            <Route path="careers" element={<Careers />} />
            <Route path="support" element={<Support />} />
            <Route path="privacy-policy" element={<PrivacyPolicy />} />
          </Routes>
        </PostProvider>
      </ScrollToTop>
      <Footer />
    </main>
  );
}

export default App;
