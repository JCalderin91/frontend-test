export interface Post {
  id?: number | string;
  author: string;
  title: string;
  description: string;
  date?: Date | string;
}

export interface PostWithId extends Post {
  id: string | number;
}

export interface PostState {
  posts: Post[];
  post: Post;
}

export type IconType =
  | "Facebook"
  | "Instagram"
  | "Pinterest"
  | "Twitter"
  | "Youtube";
